package api;

public interface IRessources {
	
	static final String[] QUESTIONS = {"Quelle est la capitale de l'Australie ?",
			"Quelle particule r�agit avec l'hydrog�ne pour former de l'eau ?",
			"Quel animal est la mascotte du navigateur Firefox ?",
			"Quel jeux vid�o met en sc�ne Thrall et Jaina ?",
			"Que cherche t'on � faire avec un alambic ?",
			"Quel personnage fictif fille d'un riche fermier r�ve d'une vie de princesse ?",
			"Que mesure un barom�tre ?",
			"Comment se nomme le dieu greque qui lance des �clairs ?",
			"Quel est le 7�me nombre premier ?",
			"Mieux vaut partir � temps que de ... ?"};
	
	static final String[] ANSWERS = {"canberra",
			"oxyg�ne",
			"panda roux",
			"warcraft",
			"distiller",
			"madame de bovary",
			"pression",
			"zeus",
			"17",
			"courir"};
}
