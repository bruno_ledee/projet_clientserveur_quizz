package impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import api.IConstants;
import api.IRessources;
import api.IServerControler;
import api.IServerView;


public class ServerControler_Impl implements IServerControler{

	private IServerView serverView;
	private EcouteThread ecouteThread;
	private ServerSocket ecouteSocket;
	private List<ServiceThread> services = new ArrayList<ServiceThread>();
	private boolean serverDown;

	@Override
	public void start() {
		serverDown = false;
		ecouteThread = new EcouteThread();
		ecouteThread.start();
	}

	public void addService(ServiceThread s) {
		synchronized (services) {
			services.add(s);
		}
	}
	private void refreshClientList() {
		List<String> clients = new ArrayList<String>();
		synchronized (services) {
			for (ServiceThread service : services)
				clients.add(service.remoteSocketAddress);
		}
		serverView.setListClients(clients);
	}
	public void endService(ServiceThread s){
		synchronized(services){
			services.remove(s);
		}
	}


	@Override
	public void end() {
		serverDown = true;
		try {
			if(ecouteSocket!=null)
				ecouteSocket.close();
		} catch (IOException e) {
		}
	}
	@Override
	public void setServerGui(IServerView serverView) {
		this.serverView = serverView;
	}

	private class EcouteThread extends Thread{

		@Override
		public void run() {
			try {
				services.clear();
				if (serverView != null)
					serverView.setListClients(null);

				ecouteSocket = new ServerSocket(8051);
				ecouteSocket.getInetAddress();
				String host = InetAddress.getLocalHost().getHostAddress();
				int port = ecouteSocket.getLocalPort();
				if (serverView != null) {
					serverView.setHostAndPort(host, port);
					serverView.log("[serveur multiclient multisession] d�marr� sur :" + host + ":" + port);
				}
				int connectId = 0;
				while (!serverDown) {
					Socket socketService = null;
					try {
						socketService = ecouteSocket.accept();
					} catch (SocketException e) {
						serverView.log("fin du serveur !");
						socketService = null;
						serverDown = true;
					}
					if (!serverDown) {
						if (socketService != null) {
							if (serverView != null)
								serverView.log("d�marrage de la session (" + (connectId - 1) + ")");
							ServiceThread s = new ServiceThread(socketService);
							if (serverView != null)
								serverView.log("RemoteSocketAddress=" + s.remoteSocketAddress);
							addService(s);
							refreshClientList();
							s.start();
						} else if (serverView != null)
							serverView.log("socket closed");
					}
				}
			} catch (IOException e) {
				if (serverView != null)
					serverView.log("[2] " + e.getMessage());
			}
		}

	}

	private class ServiceThread extends Thread{

		private Socket socketService;
		private PrintStream output;
		private BufferedReader networkIn;
		private String remoteSocketAddress = "??";

		private int questionEnCours = -1;

		public ServiceThread(Socket socketService) {
			super();
			this.socketService = socketService;
			remoteSocketAddress = socketService.getRemoteSocketAddress().toString();
		}

		private boolean open() {
			try {
				output = new PrintStream(socketService.getOutputStream(), true);
				networkIn = new BufferedReader(new InputStreamReader(socketService.getInputStream()));
			} catch (IOException e) {
				serverView.log("[1] " + e.toString());
				return false;
			}
			return true;
		}

		private void close(boolean verbose) {
			try {
				serverView.log("arr�t du service pour le client " + remoteSocketAddress);
				endService(this);
				refreshClientList();
				socketService.close();
			} catch (IOException e) {
				serverView.log("[2] " + e.toString());
				if (verbose)
					serverView.log("error while closing service " + e.toString());
			}
		}

		@Override
		public void run() {
			serverView.log("le client " + remoteSocketAddress + " s'est connect�");
			if (open()) {
				handleRequests();
				if (socketService != null && socketService.isConnected())
					close(true);
			}
		}

		private void handleRequests() {

			while (checkConnection()) {
				String requestclient = null;
				serverView.log("attente requ�te " + remoteSocketAddress);
				try {
					requestclient = networkIn.readLine();
				} catch (SocketException e) {
					requestclient = null;
				} catch (Exception e) {
					serverView.log("[3] " + e.toString());
				}
				if (requestclient == null) {
					serverView.log("le client " + remoteSocketAddress + " s'est d�connect�, fin de la session");
					break;
				}

				if(requestclient.equals(IConstants.NEW_QUESTION)){
					Random random = new Random();
					questionEnCours = random.nextInt(10);
					serverView.setGameMessage("Derni�re question envoy�e :\n" + IRessources.QUESTIONS[questionEnCours]);
					output.println(IRessources.QUESTIONS[questionEnCours]);
					output.flush();
				}

				if(requestclient.contains(IConstants.ANSWER_QUESTION)){

					if(questionEnCours!=-1){
						
						String answer = requestclient.substring(IConstants.ANSWER_QUESTION.length());
						
						if(IRessources.ANSWERS[questionEnCours].equalsIgnoreCase(answer)){
							questionEnCours = -1;
							output.println(IConstants.GOOD_ANSWER);
							output.flush();
						}
						else {
							output.println(IConstants.BAD_ANSWER);
							output.flush();
						}
						
					}

				}
				serverView.log("le client " + remoteSocketAddress + " demande: " + requestclient);

			}
		}

		private boolean checkConnection() {
			return socketService != null && !socketService.isClosed();
		}

	}
	
}
