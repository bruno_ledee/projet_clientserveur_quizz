package impl;

import api.IConstants;
import api.ISocketControler;
import api.ISocketView;

import org.eclipse.swt.SWT; 
import org.eclipse.swt.events.*;  
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData; 

public class ClientGui_Impl implements ISocketView{

	protected Shell shell;
	private Text txtInput;
	private Text txtMessage;
	private Button btNewQuestion;
	private Button btSubmit;
	private Label lblMessage;
	private Label lblResponse;

	private String host="127.0.0.1";
	private int port = 8051;
	
	private ISocketControler clientControler;
	private Label lblInfosServer;
	private Text txtServerAddress;
	private Label lblServerPort;
	private Text txtServerPort;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			ClientGui_Impl window = new ClientGui_Impl();
			window.setControler(new ClientControler_impl());
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		shell.addListener(SWT.Close, new Listener() {
			@Override
			public void handleEvent(Event event) {
				clientControler.disconnect();
				shell.dispose();
			}
		});
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	@Override
	public void setControler(ISocketControler c) {
		this.clientControler = c;
		this.clientControler.setView(this);	
	}
	@Override
	public String getAnswer(){
		return txtInput.getText();
	}
	
	@Override
	public void log(String message){
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				txtMessage.setText(txtMessage.getText() + "\r\n" + ": " + message);
				txtMessage.setTopIndex(txtMessage.getLineCount() - 1);
			}
		});
	}
	public void getViewHostAndPort(){
		host = txtServerAddress.getText();
		port = Integer.parseInt(txtServerPort.getText());
	}
	
	protected void createContents() {
		shell = new Shell();
		shell.setSize(800, 600);
		shell.setText("Client");
		shell.setLayout(new GridLayout(2, false));
		
		lblResponse = new Label(shell, SWT.NONE);
		lblResponse.setText("Votre r�ponse :");
		new Label(shell, SWT.NONE);
		
		txtInput = new Text(shell, SWT.BORDER);
		GridData gd_txtInput = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_txtInput.heightHint = 20;
		txtInput.setLayoutData(gd_txtInput);
		
		lblMessage = new Label(shell, SWT.NONE);
		lblMessage.setText("Message");
		
		lblInfosServer = new Label(shell, SWT.NONE);
		lblInfosServer.setText("server address");
		
		txtMessage = new Text(shell, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		txtMessage.setEditable(false);
		GridData gd_txtMessage = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 3);
		gd_txtMessage.widthHint = 600;
		gd_txtMessage.heightHint = 381;
		txtMessage.setLayoutData(gd_txtMessage);
		
		txtServerAddress = new Text(shell, SWT.BORDER);
		GridData gd_txtServerAddress = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtServerAddress.widthHint = 120;
		gd_txtServerAddress.heightHint = 12;
		txtServerAddress.setLayoutData(gd_txtServerAddress);
		txtServerAddress.setText(host);
		
		lblServerPort = new Label(shell, SWT.NONE);
		GridData gd_lblServerPort = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblServerPort.heightHint = 15;
		lblServerPort.setLayoutData(gd_lblServerPort);
		lblServerPort.setText("server port");
		
		txtServerPort = new Text(shell, SWT.BORDER);
		GridData gd_txtServerPort = new GridData(SWT.FILL, SWT.NONE, true, false, 1, 1);
		gd_txtServerPort.widthHint = 129;
		gd_txtServerPort.heightHint = 15;
		txtServerPort.setLayoutData(gd_txtServerPort);
		txtServerPort.setText(""+port);
		
		btNewQuestion = new Button(shell, SWT.NONE);
		btNewQuestion.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				getViewHostAndPort();
				clientControler.connect(host, port);
				clientControler.sendRequest(IConstants.NEW_QUESTION);	
			}
		});
		GridData gd_btNewQuestion = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btNewQuestion.heightHint = 31;
		gd_btNewQuestion.widthHint = 152;
		btNewQuestion.setLayoutData(gd_btNewQuestion);
		btNewQuestion.setText("Nouvelle question");
		
		btSubmit = new Button(shell, SWT.NONE);
		btSubmit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(clientControler.checkConnection()){
					clientControler.sendRequest(IConstants.ANSWER_QUESTION);
				}
				
			}
		});
		GridData gd_btSubmit = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_btSubmit.heightHint = 31;
		gd_btSubmit.widthHint = 155;
		btSubmit.setLayoutData(gd_btSubmit);
		btSubmit.setText("Submit");
		new Label(shell, SWT.NONE);
		new Label(shell, SWT.NONE);

	}
}
