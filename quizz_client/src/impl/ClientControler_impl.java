package impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import api.IConstants;
import api.ISocketControler;
import api.ISocketView;

public class ClientControler_impl implements ISocketControler{

	private ISocketView clientView;
	private Socket clientSocket;
	private String localHost;
	private String serverHost;
	private int serverPort;

	private PrintWriter out;
	private BufferedReader in;
	private SessionThread sessionThread;

	@Override
	public void setView(ISocketView view) {
		this.clientView = view;
	}

	@Override
	public boolean checkConnection() {
		return isConnected();
	}
	public boolean isConnected(){
		boolean result = clientSocket != null && !clientSocket.isClosed() && clientSocket.isConnected();
		return result;
	}

	@Override
	public boolean connect(String host, int port) {
		if(!isConnected()){
			this.serverHost = host;
			this.serverPort = port;
			try {
				openSocket();
			} catch (IOException e) {
				e.printStackTrace();
			}
			sessionThread = new SessionThread();
			sessionThread.start();
			return true;
		}
		return false;
	}
	private void openSocket() throws UnknownHostException, IOException {
		try {
			clientSocket = new Socket(serverHost, serverPort);
			localHost = clientSocket.getLocalAddress().getHostAddress();
		} catch (Exception e) {
			in = null;
			out = null;
			throw new IOException("connection error");
		}
		in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		out = new PrintWriter(clientSocket.getOutputStream(), true);
		out.println(localHost+" is connected");
		clientView.log("Connected to the server");
	}

	@Override
	public void sendRequest(String cmd) {

		if(cmd.equals(IConstants.NEW_QUESTION)){
			if(isConnected()) {
				out.println(cmd);
				out.flush();
			}
			else {
				clientView.log("connection error!");
			}

		}
		else if(cmd.equals(IConstants.ANSWER_QUESTION)) {
			String answer = clientView.getAnswer();
			
			if(isConnected()) {
				out.println(cmd+answer);
				out.flush();
			}
			else {
				clientView.log("connection error!");
			}

		}
	}

	@Override
	public void getResponse() {

		try {

			String response = in.readLine();

			if(response.equals(IConstants.GOOD_ANSWER)){
				clientView.log("Bonne r�ponse !\n");
			}
			
			else if(response.contains("?")){
				clientView.log(response+"\n");

			}
			else if(response.equals(IConstants.BAD_ANSWER)){
				clientView.log("Mauvaise r�ponse, r�essayez");

			}

		} catch (IOException e) {

		}
	}

	@Override
	public void disconnect() {
		if(isConnected())closeSocket();	
	}
	private void closeSocket() {
		if (clientSocket != null)
			try {
				clientSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (in != null)
			try {
				in.close();
				in = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (out != null) {
			out.close();
			out = null;
		}
	}

	class SessionThread extends Thread {

		@Override
		public void run() {
			while (isConnected()) {
				getResponse();
			}
			disconnect();
		}
	}
}
