package api;

public interface IConstants {
	static final String NEW_QUESTION = "new question";
	static final String ANSWER_QUESTION = "response";
	static final String GOOD_ANSWER = "good answer";
	static final String BAD_ANSWER = "bad answer";
}
