package api;


public interface ISocketControler {
	public void setView(ISocketView view);
	public boolean checkConnection();
	public boolean connect(String host, int port);
	public void sendRequest(String cmd);
	public void getResponse();
	public void disconnect();
}
