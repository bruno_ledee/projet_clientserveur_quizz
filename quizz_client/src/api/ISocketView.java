package api;

public interface ISocketView {
	public void setControler(ISocketControler c);
	public String getAnswer();
	public void log(String message);
}
